﻿using System.Diagnostics;
using Newtonsoft.Json.Linq;
using PuppeteerSharp;
using PuppeteerSharp.Input;

namespace PuppeteerPlus;

public static class PuppeteerExtensions
{
    /// <summary>
    ///     Wait for a naw page to open and return it
    /// </summary>
    /// <param name="page"></param>
    /// <param name="timeout"> Maximum time to wait for in milliseconds. Pass 0 to disable timeout. </param>
    /// <returns>The newly opened page</returns>
    // https://stackoverflow.com/a/57766136
    public static async Task<IPage?> WaitNewPageAsync(this IPage page, int? timeout = null)
    {
        // Save target of original page to know that this was the opener:     
        var pageTarget = page.Target;
        
        // Check that the first page opened this new page:
        var newTarget = await pageTarget.Browser.WaitForTargetAsync(
            target => target.Opener == pageTarget,
            new WaitForOptions() { Timeout = timeout }
        );
        
        // Get the new page object:
        return await newTarget.PageAsync();
    }

    /// <summary>
    ///     Check if a element is visible inside the HTML document.
    ///     This method replaces the unreliable IsIntersectingViewportAsync method.
    /// </summary>
    /// <param name="elementHandle">The element to check</param>
    /// <returns>If visible</returns>
    // https://github.com/puppeteer/puppeteer/issues/4356#issuecomment-487330171
    public static Task<bool> IsVisibleAsync(this IElementHandle elementHandle) =>
        elementHandle.EvaluateFunctionAsync<bool>(@"(element) => {
                if (!element || element.offsetParent === null)
                    return false;

                const style = window.getComputedStyle(element);
                return style && style.display !== 'none' && style.visibility !== 'hidden' && style.opacity !== '0';
            }");

    /// <summary>
    ///     Wait until the page size does not change for a short time.
    ///     It is recommended to avoid using this method. (Could be deprecated later)
    /// </summary>
    /// <param name="page"></param>
    /// <param name="timeout">Deadline before cancellation</param>
    // https://stackoverflow.com/a/61304202
    [Obsolete("This method is slower, and not efficient. Use WaitForPageToLoadAsync() instead.")]
    public static async Task WaitTillHTMLRenderedAsync(this IPage page, int timeout = 30000)
    {
        const int checkDurationMsecs = 250;
        var cancellationToken = new CancellationTokenSource(timeout);
        var lastHTMLSize = 0;
        var countStableSizeIterations = 0;
        const int minStableSizeIterations = 3;

        while (!cancellationToken.IsCancellationRequested)
            try
            {
                var html = await page.GetContentAsync();
                var currentHTMLSize = html.Length;

                if (lastHTMLSize != 0 && currentHTMLSize == lastHTMLSize)
                    countStableSizeIterations++;
                else
                    countStableSizeIterations = 0; //reset the counter

                if (countStableSizeIterations >= minStableSizeIterations) break;

                lastHTMLSize = currentHTMLSize;
                await page.WaitForTimeoutAsync(checkDurationMsecs);
            }
            catch (EvaluationFailedException)
            {
            }
    }

    /// <summary>
    ///     Wait for a page to exit
    /// </summary>
    /// <param name="page"></param>
    /// <param name="targetUrl">The URL or start URL to wait for his exit</param>
    /// <param name="timeout">Time to wait before stop waiting</param>
    public static Task WaitForPageToExitAsync(this IPage page, string? targetUrl = null, in int timeout = 30000)
    {
        var tokenSource = timeout > 0 ? new CancellationTokenSource(timeout) : new CancellationTokenSource();
        targetUrl ??= page.Url;

        return WaitForPageToExitAsync(page, targetUrl, tokenSource.Token);
    }

    /// <summary>
    ///     Wait for a page to exit
    /// </summary>
    /// <param name="page"></param>
    /// <param name="targetUrl">The URL or start URL to wait for his exit</param>
    /// <param name="token">Cancellation token</param>
    public static async Task WaitForPageToExitAsync(this IPage page, string targetUrl, CancellationToken token)
    {
        while (page.Url.StartsWith(targetUrl))
            await Task.Delay(500, token);
    }

    /// <summary>
    ///     Try navigates to an url and promise that the navigation work.
    /// This dosn't work if the page do a redirection
    /// </summary>
    /// <param name="page"></param>
    /// <param name="url">URL to navigate page to. The url should include scheme, e.g. https://.</param>
    /// <param name="options">Navigation parameters.</param>
    /// <param name="maxTry">Number of time to retry when navigation fail</param>
    /// <returns>If navigation success</returns>
    public static async Task<bool> TryGoToAsync(this IPage page, string url, NavigationOptions options, int maxTry = 10)
    {
        IResponse response;
        var trys = 0;

        if (string.IsNullOrEmpty(url))
            return false;
        
        while (maxTry >= trys)
        {
            try
            {
                response = await page.GoToAsync(url, options);
                if (response is { Ok: true } or null)
                    return true;

                // May not be needed
                /*else if (response == null)  // NULL can happen when there is a redirection before, TODO: Check if the target page has been loaded
                    if (page.Url.StartsWith(url))
                        return true;
                    else
                    {
                        Debug.WriteLine("wait for: " + page.Url);
                        await page.WaitForPageToExit();
                        if (page.Url.StartsWith(url))
                            return true;
                    }*/
            }
            catch (Exception)
            {
                // Sometime the page load, but puppeteer was not able to know if the page has loaded
                if (page.Url.StartsWith(url))
                {
                    await page.WaitForPageToLoadAsync();
                    //await page.WaitTillHTMLRenderedAsync(); // Don't wait the page has fully loaded
                    return true;
                }
            }

            trys++;
        }

        return false;
    }

    /// <summary>
    ///     Try navigates to an url and promise that the navigation work.
    /// </summary>
    /// <param name="page"></param>
    /// <param name="url">URL to navigate page to. The url should include scheme, e.g. https://.</param>
    /// <param name="timeout">Maximum navigation time in milliseconds, defaults to 30 seconds, pass 0 to disable timeout.</param>
    /// <param name="waitUntil">
    ///     When to consider navigation succeeded, defaults to PuppeteerSharp.WaitUntilNavigation.Load.
    ///     Given an array of PuppeteerSharp.WaitUntilNavigation, navigation is considered
    ///     to be successful after all events have been fired
    /// </param>
    /// <returns>If navigation success</returns>
    public static Task<bool> TryGoToAsync(
            this IPage page, string url, int? timeout = null, WaitUntilNavigation[]? waitUntil = null) =>
        page.TryGoToAsync(
            url,
            new NavigationOptions
            {
                Timeout = timeout,
                WaitUntil = waitUntil
            },
            1);


    /// <summary>
    ///     Try navigates to an url and promise that the navigation work.
    /// </summary>
    /// <param name="page"></param>
    /// <param name="url">URL to navigate page to. The url should include scheme, e.g. https://.</param>
    /// <param name="waitUntil">When to consider navigation succeeded.</param>
    /// <param name="maxTry">Number of time to retry when navigation fail</param>
    /// <returns>If navigation success</returns>
    public static Task<bool> TryGoToAsync(
            this IPage page, in string url, in WaitUntilNavigation waitUntil, in int maxTry = 10) =>
        page.TryGoToAsync(
            url,
            new NavigationOptions
            {
                WaitUntil = new[] { waitUntil }
            },
            maxTry);

    /// <summary>
    ///     Clear the text annd sends a keydown, keypress/input, and keyup event for each character in the text.
    /// </summary>
    /// <param name="page"></param>
    /// <param name="selector">
    ///     A selector to search for element to focus. If there are multiple elements satisfying the selector,
    ///     the first will be focused.
    /// </param>
    /// <param name="text">A text to type into a focused element</param>
    /// <param name="options">type options</param>
    /// <returns>Task</returns>
    public static async Task ReplaceAllTextAsync(
        this IPage page, string selector, string text, TypeOptions? options = null)
    {
        IElementHandle element;

        while (true)
        {
            // Select all
            await page.FocusAsync(selector);
            await page.Keyboard.DownAsync("Control");
            await page.Keyboard.PressAsync("A");
            await page.Keyboard.UpAsync("Control");
            // Delete all
            await page.Keyboard.PressAsync("Backspace");
            // Write
            await page.Keyboard.TypeAsync(text, options);
            
            // Check text
            element = await page.QuerySelectorAsync(selector);
            if (element != null)
            {
                var value = await element.GetPropertyValueAsync<string>("value");

                if (value != text)
                    continue;
            }

            return;
        }
    }

    /// <summary>
    ///     Waits for a selector to hide
    /// </summary>
    /// <param name="page"></param>
    /// <param name="selector">A selector of an element to wait for</param>
    /// <param name="timeout">The time span to wait before canceling the wait</param>
    /// <param name="visibilityCheck">wait until the element is not visible instead of disappearing</param>
    /// <returns>Task</returns>
    public static Task<bool> WaitForSelectorToHideAsync(
        this IPage page, in string selector, in bool visibilityCheck = false, in int timeout = 30000)
    {
        var tokenSource = timeout > 0 ? new CancellationTokenSource(timeout) : new CancellationTokenSource();
        
        return WaitForSelectorToHideAsync(page, selector, tokenSource.Token, visibilityCheck);
    }

    /// <summary>
    ///     Waits for a selector to hide
    /// </summary>
    /// <param name="page"></param>
    /// <param name="selector">A selector of an element to wait for</param>
    /// <param name="cancellationToken"></param>
    /// <param name="visibilityCheck">wait until the element is not visible instead of disappearing</param>
    /// <returns>Task</returns>
    public static async Task<bool> WaitForSelectorToHideAsync(
        this IPage page, string selector, CancellationToken cancellationToken, bool visibilityCheck = false)
    {
        IElementHandle element;

        while (!cancellationToken.IsCancellationRequested)
            try
            {
                element = await page.QuerySelectorAsync(selector);
                if (element != null)
                {
                    if (visibilityCheck && !await element.IsVisibleAsync())
                        return true;

                    await Task.Delay(500);
                }
                else return true;
            }
            catch (PuppeteerException e) { break; }
            catch (NullReferenceException) { break; }

        return false;
    }

    /// <summary>
    /// Remove an element
    /// </summary>
    /// <param name="page"></param>
    /// <param name="selector">Selector to query the element to remove</param>
    /// <returns></returns>
    public static Task<JToken> RemoveAsync(this IPage page, in string selector) =>
        page.EvaluateFunctionAsync(@"(selector) => {
			    var elements = document.querySelectorAll(selector);

			    for (var i=0; i< elements.length; i++) {
				    elements[i].parentNode.removeChild(elements[i]);
			    }
		    }", selector);

    /// <summary>
    ///     Removes the element from the DOM.
    /// </summary>
    /// <param name="element">Element to remove</param>
    /// <returns>Task</returns>
    public static Task RemoveAsync(this IElementHandle element) =>
        element.EvaluateFunctionAsync<string?>(@"(element) => {
                element.remove()
            }");

    /// <summary>
    ///     Output the JavaScript browser console into the app debugger
    /// </summary>
    /// <param name="page"></param>
    public static void SetConsoleOutputToDebugger(this IPage page) =>
        page.Console += async (_, msg) =>
        {
            const string TAG = "[JS Console] ";

            if (msg.Message.Args != null)
                foreach (var msgArg in msg.Message.Args)
                    try
                    {
                        Debug.WriteLine(TAG + await msgArg.JsonValueAsync());
                    }
                    catch { }

            /*if (msg.Message.Text != null)
                Debug.WriteLine(TAG + msg.Message.Text);*/
        };

    /// <summary>
    ///     Get the attribute of an element.
    ///     If the given attribute does not exist, the value returned will be null.
    /// </summary>
    /// <param name="element">The element</param>
    /// <param name="attributeName">The name of the attribute whose value you want to get</param>
    /// <returns>The value of a specified attribute on the element</returns>
    public static Task<string?> GetAttributeAsync(this IElementHandle? element, in string attributeName) =>
        element.EvaluateFunctionAsync<string?>(@"(element, attributeName) => {
                return element.getAttribute(attributeName)
            }", attributeName);

    /// <summary>
    ///     Fetches a single property value from the queried object
    /// </summary>
    /// <typeparam name="T">Value object type</typeparam>
    /// <param name="page"></param>
    /// <param name="propertyName">Property to get</param>
    /// <param name="selector">A selector to query page for</param>
    /// <returns>The value of the property from the queried element</returns>
    public static Task<T> GetPropertyValueAsync<T>(this IPage page, string propertyName, in string selector) =>
        page.QuerySelectorAsync(selector)
                   .ContinueWith(element =>
                                     element.Result.GetPropertyValueAsync<T>(propertyName))
                   .Unwrap();

    /// <summary>
    ///     Fetches a single property value from the referenced object
    /// </summary>
    /// <typeparam name="T">Value object type</typeparam>
    /// <param name="element"></param>
    /// <param name="propertyName">Property to get</param>
    /// <returns>The value of the property from the given element</returns>
    public static Task<T> GetPropertyValueAsync<T>(this IElementHandle element, in string propertyName) =>
        element.GetPropertyAsync(propertyName)
                      .ContinueWith(e => e.Result.JsonValueAsync<T>())
                      .Unwrap();

    /// <summary>
    ///     Will wait until the document readyState is 'complete'.
    ///     This mean that the document and all sub-resources such as scripts, images, stylesheets and frames have finished
    ///     loading.
    /// </summary>
    /// <param name="page"></param>
    // https://developer.mozilla.org/docs/Web/API/Document/readyState
    // https://stackoverflow.com/a/52610561/11873025
    public static Task WaitForPageToLoadAsync(this IPage page) =>
        page.EvaluateFunctionAsync(@"async () => {
                let ready = document.readyState === 'complete';

                document.onreadystatechange = () => {
                    ready = document.readyState === 'complete'
                };

                while (true) {
                    if (ready) { return };
                    await new Promise(resolve => setTimeout(resolve, 100)); // prevents app from hanging
                }
            }");

    /// <summary>
    /// Gets the user agent that is used in this page
    /// </summary>
    /// <param name="page"></param>
    /// <returns>Specific user agent that is used in this page</returns>
    public static Task<string> GetUserAgentAsync(this IPage page) =>
        page.EvaluateFunctionAsync<string>(@"() => {
            return window.navigator.userAgent
        }");
}