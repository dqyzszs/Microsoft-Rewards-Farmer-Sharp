[English 🇬🇧](./README.md) | [Français 🇫🇷](./README.fr.md)

# Microsoft Rewards Points Farmer  

Accumule des points Microsoft Rewards automatiquement.  

Veuillez lire les [Mentions légales](#legal) avant l'utilisation.  

Fonctionne sur Windows, Linux et *macOS*  

## Communauté

Discutez avec la communauté sur Gitter : https://app.gitter.im/#/room/#ms-rewards-farmer:gitter.im
Ou sur d'autre clients matrix : https://matrix.to/#/#ms-rewards-farmer:gitter.im

⚠️ **Il n'y aura plus de serveur Discord, car ce programme n'est pas conforme aux [directives de la communauté Discord](https://discord.com/guidelines) à la section "Soyez honnête "** ⚠️
Probablement parce que même si cette application n'est pas illégale, elle peut techniquement nuire à Microsoft et vous pouvez obtenir un gain d'une manière malhonnête, mais la façon dont ils l'ont écrit n'est pas très précise... et peut donner lieu à de mauvaises interprétations...

## Installation

Vous pouvez consulter le PDF réalisé par Cappy ? du serveur Discord banni [ici](/Docs/how_to_use_msrewardsfarmersharp.pdf). (PDF en anglais)

1. Téléchargez un des [fichiers Zip](https://github.com/Tom60chat/Microsoft-Rewards-Farmer-Sharp/releases) qui correspond à votre [système](https://whatsmyos-com.translate.goog/?_x_tr_sl=en&_x_tr_tl=fr&_x_tr_hl=fr&_x_tr_pto=wapp) et extrayez-le quelque part (comme votre bureau ou un autre dossier, n'ouvrez pas l'appli directement à partir du fichier Zip).  

2. Modifiez le fichier `Settings.json` dans le dossier extrait en fournissant les informations de votre compte Microsoft dans la section `Accounts`.
   - Username - L'adresse mail ou le nom d'utilisateur du compte Microsoft.  
   - Password - Le mot de passe du compte Microsoft  
   - OTP - Le code secret OTP (One Time Password) pour l'authentification à deux facteurs (2FA).  
   - Encoding - Type d'encodage :  
     - Laissez vide pour mettre du texte brut  
     - Base64
    
   Vous pouvez donc mettre dans `Accounts` :
   - Seulement Username et Password
     ```json
     {
       "Username": "you@domain.com",
       "Password": "yourPassword"
     }
     ```
   - Username et Password avec OTP
     ```json
     {
       "Username": "youSecond@domain.com",
       "Password": "yourSecondPassword",
       "OTP": "ABC123DEF456GH78"
     }
     ```
   - Username et Password avec OTP, dont les 2 derniers sont encodés en Base64
     ```json
     {
       "Username": "you2@domain.com",
       "Password": "eW91cl9wYXNzd29yZA==",
       "OTP": "QUJDMTIzREVGNDU2R0g3OA==",
       "Encoding": "Base64"
      }
     ```
   
   Voici un exemple simple :
   ```json
    {
      "Accounts": [
        {
          "Username": "mon_premier_compte@domain.com",
          "Password": "mots_de_passe_de_mon_premier_compte"
        },
        {
          "Username": "mon_second_comptet@domain.com",
          "Password": "mots_de_passe_de_mon_deuxième_compte"
        }
      ]
    }
    ```
   Le deuxième compte est facultatif et peut être supprimé.
   Utilisez autant de comptes que vous voulez, mais rappelez-vous que chaque compte utilise autant de puissance CPU qu’une seule fenêtre de navigateur.

## Exécution  

Lancez `Microsoft Rewards Farmer(.exe)` dans le dossier extrait.  
  
Arguments :  
 - **Mode Tête** `-nh`, `--no-headless`  
   Affiche les fenêtres du navigateur Web, réduisant ainsi l'impact sur le processeur.  

 - **Sans session:** `-ns`, `--no-session`  
   Désactive le système de mise en cache de session.   

 - **Type de navigateur**: `-br`, `--browser` NOMFICHIER_OU_TYPE
   Spécifie le type de navigateur à utiliser :  
   - User - Utiliser le navigateur actuellement installé (par défaut si il est disponible et compatible).  
   - [Chemin vers l'exécutable d'un navigateur] - Pour forcer MRF à utiliser un navigateur  
   - Built-in - Utiliser le navigateur fourni par puppeteer (par défaut si aucune des options ci-dessus ne fonctionne)  

 - **Limite de fermes**: `-n`, `--farms-limit` NOMBRE  
   Définit le nombre de comptes qui peuvent fonctionner en même temps.  
   - 0 -> Illimité (par défaut)  

 - **Batch**: `-b`, `--batch`  
  Exécute le programme en mode batch, c'est-à-dire sans attente pour appuyer sur la touche Entrée à la fin du programme.

 - **Fichier de paramétrage**: `-s`, `--settings` NOMFICHIER
  Charge le fichier de paramétrages donné au lieu du fichier par défaut.

 - **Arguments Chrome**: `-a`, `--overwrite-chrome-argument` ARGUMENTS (séparés par des virgules)
  Remplace les arguments par défaut de Chrome par ceux donnés.  
  Exemple: `-a --incognito,--no-sandbox,--window-size="800,680"`

 - **Délai d'expiration**: `-t`, `--timeout` NOMBRE  
  Définit le délai d'expiration en millisecondes pour les requêtes HTTP.  
  - -1 -> Utilise le délai d'expiration par défaut (par défaut)  
  - 0 -> Pas de délai d'expiration

 - **Aide**: -h, --help
  Montre cela.
   
### Comment utiliser les arguments ?

[BleepingComputer: Comprendre les arguments de la ligne de commande et comment les utiliser](https://www-bleepingcomputer-com.translate.goog/tutorials/understanding-command-line-arguments-and-how-to-use-them/?_x_tr_sl=en&_x_tr_tl=fr&_x_tr_hl=fr&_x_tr_pto=wapp) ou regardez [#23](https://github-com.translate.goog/Tom60chat/Microsoft-Rewards-Farmer-Sharp/issues/23?_x_tr_sl=fr&_x_tr_tl=en&_x_tr_hl=fr&_x_tr_pto=wapp)  
 
## Farming

- Cette application permet de récolter environ 250 points par jour et par compte.
- Vous pouvez farmer deux fois en utilisant un VPN (par exemple, une fois dans votre pays et une fois dans le pays disponible le plus proche).
  - Vous pouvez utiliser [ProtonVPN](https://protonvpn.com/), il a un abonnement gratuit disponible avec une vitesse suffisante. (Non sponsorisé)
- Cela signifie qu'avec 5 comptes, vous pouvez obtenir 15.000 points par compte en un mois, ce qui représente environ 50€ de carte cadeau Amazon par mois.

## <a name='legal'/> Mentions légales
Je ne suis pas responsable de ce que vous faites avec ce programme, UTILISEZ-LE À VOTRE PROPRE RISQUE !  
Ce programme est conçu à des fins éducatives, et j'ai beaucoup appris sur l'utilisation de puppetter 😋  

L'utilisation de cette application sur les services Microsoft peut amener Microsoft à vous disqualifier, à désactiver votre accès au programme ou à votre compte Rewards.  
Veuillez lire https://www.microsoft.com/fr-fr/servicesagreement/ en particulier la section sur Microsoft Rewards.  

Sous la licence Unlicense
