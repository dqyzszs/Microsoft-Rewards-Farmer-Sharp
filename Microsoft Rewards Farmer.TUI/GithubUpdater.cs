﻿#nullable enable

using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Octokit;

public class GithubUpdater
{
    #region Constructors

    public GithubUpdater(string repositoryOwner, string repositoryName)
    {
        _repositoryOwner = repositoryOwner;
        _repositoryName = repositoryName;
    }

    #endregion

    #region Variables

    private readonly string _repositoryOwner;
    private readonly string _repositoryName;
    private string? _lastTag;

    #endregion

    #region Methods

    /// <summary>
    ///     Check if a new release is available on GitHub
    /// </summary>
    /// <param name="includePrerelease">Include the latest pre-release</param>
    /// <returns>Return <see langword="true" /> if a new release is available, else <see langword="false" /></returns>
    public async Task<bool> CheckNewerVersionAsync(bool includePrerelease = false)
    {
        // Get all releases from GitHub
        // Source: https://octokitnet.readthedocs.io/en/latest/getting-started/
        try
        {
            var client = new GitHubClient(new ProductHeaderValue(_repositoryName));
            var releases = await client.Repository.Release.GetAll(_repositoryOwner, _repositoryName);

            foreach (var release in releases)
            {
                // Skip prerelease if we don't exclude them
                if (!includePrerelease && release.Prerelease)
                    continue;

                // Setup the versions
                var localVersion = Assembly.GetExecutingAssembly().GetName().Version;

                if (!TryStringToVersion(release.TagName, out var latestGitHubVersion) &&
                    !TryStringToVersion(release.Name, out latestGitHubVersion))
                    return false;

                // Compare the Versions
                // Source: https://stackoverflow.com/a/7568182
                var versionComparison = localVersion?.CompareTo(latestGitHubVersion) ?? 0;

                _lastTag = release.TagName;

                // If the version on GitHub is more up to date than this local release we return true, else false
                return versionComparison < 0;
            }
        }
        catch (Exception ex)
        {
            Debug.WriteLine("We were not able to search for a new version on GitHub");
            Debug.WriteLine(ex.Message);
        }

        return false;
    }

    /// <summary>
    ///     Open a link to the latest release
    /// </summary>
    /// <exception cref="Exception"></exception>
    public void Update()
    {
        var url = string.IsNullOrEmpty(_lastTag)
            ? $"https://github.com/{_repositoryName}/{_repositoryName}/releases"
            : $"https://github.com/{_repositoryName}/{_repositoryName}/releases/tag/{_lastTag}";

        // Source: https://brockallen.com/2016/09/24/process-start-for-urls-on-net-core/
        try
        {
            Process.Start(url);
        }
        catch
        {
            // Hack because of this: https://github.com/dotnet/corefx/issues/10361
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                url = url.Replace("&", "^&");
                Process.Start(new ProcessStartInfo("cmd", $"/c start {url}") { CreateNoWindow = true });
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                Process.Start("xdg-open", url);
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                Process.Start("open", url);
            }
            else
            {
                throw;
            }
        }
    }

    private static bool TryStringToVersion(string versionName, out Version? version)
    {
        if (versionName.ToLower().StartsWith("v"))
            versionName = versionName[1..]; // Remove the first character

        var hyphenIndex = versionName.IndexOf('-');
        if (hyphenIndex != -1)
            versionName = versionName[..hyphenIndex]; // Remove everything after -

        return Version.TryParse(versionName, out version);
    }

    #endregion
}