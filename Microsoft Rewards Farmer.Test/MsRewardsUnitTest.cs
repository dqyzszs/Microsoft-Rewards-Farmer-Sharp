﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuppeteerSharp;
using Xunit;
using Xunit.Abstractions;

namespace MicrosoftRewardsFarmer.Test;

public class MsRewardsUnitTest : UnitTest
{
    #region Constructors

    public MsRewardsUnitTest(ITestOutputHelper output) : base(output) { }

    #endregion


    #region Methods

    /// <summary>
    /// REQUIRE ACCOUNT
    /// </summary>
    [Fact]
    public async void GetRewardsPointsTest()
    {
        var page = await GetNewPageAsync();
        var progress = GetProgress();
        var account = GetAccount();

        await Bing.SignInToMicrosoftAsync(page, account, progress);

        var points = await MsRewards.GetRewardsPointsAsync(page, progress);
        
        Assert.NotEqual(0, points);
        
        output.WriteLine(points.ToString());
    }

    /// <summary>
    /// REQUIRE ACCOUNT
    /// </summary>
    [Fact]
    public async void GetRewardsSearchPointsTest()
    {
        var page = await GetNewPageAsync();
        var progress = GetProgress();
        var account = GetAccount();

        await Bing.SignInToMicrosoftAsync(page, account, progress);

        var searchesPoints = await MsRewards.GetRewardsSearchPointsAsync(page, progress);
        
        Assert.NotNull(searchesPoints);
        
        // Level 1 account don't have mobile search
        //Assert.NotEqual(0, searchesPoints.Value.Total.MobileSearch);
        Assert.NotEqual(0, searchesPoints.Value.Total.DesktopSearch);
        Assert.NotEqual(0, searchesPoints.Value.Total.EdgeSearch);
        
        output.WriteLine(searchesPoints.Value.ToString());
    }

    /// <summary>
    /// REQUIRE ACCOUNT
    /// </summary>
    [Fact]
    public async void GetCardsTest()
    {
        var page = await GetNewPageAsync();
        var progress = GetProgress();
        var account = GetAccount();

        await Bing.SignInToMicrosoftAsync(page, account, progress);

        var cards = await MsRewards.GetCardsAsync(page, progress);

        output.WriteLine(cards.Length.ToString());
    }

    /// <summary>
    /// REQUIRE ACCOUNT
    /// </summary>
    [Fact]
    public async void ProceedCardsTest()
    {
        var page = await GetNewPageAsync();
        var progress = GetProgress();
        var account = GetAccount();
        
        await Bing.SignInToMicrosoftAsync(page, account, progress);
        
        var cards = await MsRewards.GetCardsAsync(page, progress);

        if (cards.Length <= 0)
        {
            output.WriteLine("No cards to process");
            return;
        }
        
        await MsRewards.ProceedCardsAsync(page, cards, progress);

        var remainingCards = await MsRewards.GetCardsAsync(page, progress);
            
        Assert.Empty(remainingCards);
    }

    /// <summary>
    /// REQUIRE ACCOUNT
    /// </summary>
    [Fact]
    public async void GoToMicrosoftRewardsPageTest()
    {
        var page = await GetNewPageAsync();
        var progress = GetProgress();
        var account = GetAccount();
        
        await Bing.SignInToMicrosoftAsync(page, account, progress);

        await MsRewards.GoToMicrosoftRewardsPageAsync(page);

        Assert.StartsWith(MsRewards.URL, page.Url);
    }

    [Theory]
    [InlineData("Quiz", Bing.URL + "search?q=Nouvelles%20technologies&rnoreward=1&mkt=FR-FR&FORM=ML12JG&skipopalnative=true&rqpiodemo=1&filters=BTEPOKey:%22REWARDSQUIZ_FRFR_MicrosoftRewardsQuizCB_20211130%22%20BTROID:%22Gamification_DailySet_FRFR_20211130_Child2%22%20BTROEC:%220%22%20BTROMC:%2230%22")]
    [InlineData("Quick quiz", Bing.URL + "search?q=qu%27est-ce%20que%20Z%20Event&rnoreward=1&mkt=FR-FR&FORM=ML12JG&skipopalnative=true&rqpiodemo=1&filters=BTEPOKey:%22REWARDSQUIZ_FRFR_MicrosoftRewardsQuizDS_20211201%22%20BTROID:%22Gamification_DailySet_FRFR_20211201_Child2%22%20BTROEC:%220%22%20BTROMC:%2230%22")]
    [InlineData("50/50", Bing.URL + "search?q=langue%20fran%c3%a7aise&rnoreward=1&mkt=FR-FR&FORM=ML12JG&skipopalnative=true&rqpiodemo=1&filters=BTEPOKey:%22REWARDSQUIZ_FR-FR_ThisOrThat_FrenchLangCountries_EB_20211129%22%20BTROID:%22Gamification_DailySet_FRFR_20211129_Child2%22%20BTROEC:%220%22%20BTROMC:%2250%22%20BTROQN:%220%22")]
    //  Expired link
    // [InlineData("Poll", Bing.URL + "search?q=forets%20en%20france&rnoreward=1&mkt=FR-FR&skipopalnative=true&form=ML17QA&filters=PollScenarioId:%22POLL_FRFR_RewardsDailyPoll_20211207%22%20BTROID:%22Gamification_DailySet_FRFR_20211207_Child3%22%20BTROEC:%220%22%20BTROMC:%2210%22")]
    [InlineData("Xbox store", "https://www.microsoft.com/fr-fr/store/b/xbox?ocid=MSRewards&form=ML27YL&OCID=ML27YL&PUBL=RewardsDO&PROGRAMNAME=MicrosoftStore&CREA=ML27YL")]
    [InlineData("Weekly Quiz", Bing.URL + "search?q=Bing%20homepage%20quiz&rnoreward=1&mkt=EN-US&skipopalnative=true&form=ML17QA&filters=BTROID:%22Gamification_DailySet_20230114_Child2%22%20BTROEC:%220%22%20BTROMC:%2210%22")]
    public async void ProceedCardAsync(string cartTypeName, string cardUrl)
    {
        var page = await GetNewPageAsync();

        await page.GoToAsync(cardUrl, WaitUntilNavigation.Networkidle0);

        await MsRewards.ProceedCardAsync(page);
    }
    #endregion
}

