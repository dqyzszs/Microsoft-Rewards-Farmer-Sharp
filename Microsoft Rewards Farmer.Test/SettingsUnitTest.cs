using System.IO;
using System.Net;
using MicrosoftRewardsFarmer.Exceptions;
using MicrosoftRewardsFarmer.Models;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;

namespace MicrosoftRewardsFarmer.Test;

public class SettingsUnitTest : UnitTest
{
    #region Constructors
    
    public SettingsUnitTest(ITestOutputHelper output) : base(output)
    {
    }

    #endregion
    
    #region Methods

    [Theory]
    [InlineData("invalidPath", true)]
    [InlineData("1.json", true)]
    [InlineData("2.json", true)]
    [InlineData("3.json", true)]
    [InlineData("4.json", true)]
    [InlineData("5.json", false)]
    [InlineData("6.json", false)]
    [InlineData("7.json", true)]
    public void GetSettingsTest(string settingsFile, bool shouldFail)
    {
        try
        {
            Settings.GetSettings("Resources/Settings test files/" + settingsFile);
        
            Assert.False(shouldFail);
        }
        catch (SettingsLoadException ex)
        {
            output.WriteLine(ex.Message);
            
            Assert.True(shouldFail);
        }
        catch (JsonSerializationException ex)
        {
            output.WriteLine(ex.Message);
            
            Assert.True(shouldFail);
        }
    }
    

    #endregion
}