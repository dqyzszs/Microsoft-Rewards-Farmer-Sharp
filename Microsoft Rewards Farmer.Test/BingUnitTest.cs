using MicrosoftRewardsFarmer.Models;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using MicrosoftRewardsFarmer.Extensions;
using Xunit;
using Xunit.Abstractions;

namespace MicrosoftRewardsFarmer.Test;

public class BingUnitTest : UnitTest
{
    #region Constructors

    public BingUnitTest(ITestOutputHelper output) : base(output) { }

    #endregion


    #region Methods

    /*[Fact]
    public async void IsConnected()
    {
        var browser = await GetBrowserAsync();
        var page = await browser.NewPageAsync();

        await Bing.GoToBingAsync(page);

        var result = await Bing.IsConnected(page);
        
        Assert.False(result);
    }*/

    /// <summary>
    /// REQUIRE ACCOUNT
    /// </summary>
    [Fact]
    public async void SignInToMicrosoftTest()
    {
        var page = await GetNewPageAsync();
        var account = GetAccount();
        var progress = GetProgress();

        await Bing.SignInToMicrosoftAsync(page, account, progress);
    }

    [Fact]
    public async void RunSearchesTest()
    {
        var page = await GetNewPageAsync();
        var progress = GetProgress();

        await Bing.RunSearchesAsync(page, 10, progress);
    }

    [Fact]
    public async void GoToBingTest()
    {
        var page = await GetNewPageAsync();
        var progress = GetProgress();

        var result = await Bing.GoToBingAsync(page);

        Assert.True(result);
        Assert.StartsWith(Bing.URL, page.Url);
    }

    [Fact]
    public async void CheckBingReadyTest()
    {
        var page = await GetNewPageAsync();

        await page.GoToAsync(Bing.URL);

        await Bing.CheckBingReadyAsync(page, false);

        await page.GoToAsync(Bing.URL + "search?q=test");

        await Bing.CheckBingReadyAsync(page, false);

        await page.SwitchToMobileAsync();
        await page.ReloadAsync();

        await Bing.CheckBingReadyAsync(page, false);
    }

    #endregion

}