﻿using System.Text;

namespace MicrosoftRewardsFarmer.Utilities;

/// <summary>
///     Converter / Decoder
/// </summary>
internal class Converter
{
    /// <summary>
    ///     Convert Base64 string to plain text string
    /// </summary>
    /// <param name="base64">Base64 string</param>
    /// <returns>Plain text string</returns>
    public static string Base64_to_string(string base64)
    {
        try
        {
            var array = Convert.FromBase64String(base64);

            return Encoding.UTF8.GetString(array);
        }
        catch (Exception)
        {
            return base64;
        }
    }

    /// <summary>
    ///     Convert hexadecimal in string format to bytes
    /// </summary>
    /// <param name="hex">Hexadecimal in string format</param>
    /// <returns></returns>
    /// <exception cref="System.ArgumentException" />
    /// <exception cref="System.ArgumentOutOfRangeException" />
    /// <exception cref="System.FormatException" />
    /// <exception cref="System.OverflowException" />
    //https://stackoverflow.com/a/321404/11873025
    public static byte[] string_to_byte_array(string hex)
    {
        return Enumerable.Range(0, hex.Length)
                         .Where(x => x % 2 == 0)
                         .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                         .ToArray();
    }
}