﻿using System.Diagnostics;

namespace MicrosoftRewardsFarmer.Utilities;

public class ExternalProcessManagement
{
    /// <summary>
    ///     Kill this process when the application has been killed
    /// </summary>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool RegisterForClosing(Process target)
    {
        try
        {
            var targetId = target.Id;
            var contractorId = Environment.ProcessId;

            var hitmanFolderPath = AppPath.GetFullPath("Hitman") ??
                                   throw new ArgumentNullException("AppPath.GetFullPath(\"Hitman\")");
            var hitmanFolder = new DirectoryInfo(hitmanFolderPath);
            if (hitmanFolder == null) throw new ArgumentNullException(nameof(hitmanFolder));

            if (!hitmanFolder.Exists) return false;

            var hitmanSubfolders = hitmanFolder.GetDirectories() ??
                                   throw new ArgumentNullException("hitman_folder.GetDirectories()");

            if (hitmanSubfolders.Length == 0) return false;

            var hitmanFiles = hitmanSubfolders[0].GetFiles("Hitman*");

            if (hitmanFiles.Length == 0) return false;

            var hitmanPath = Path.GetFullPath(hitmanFiles[0].FullName);
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = hitmanPath,
                    Arguments = $"{contractorId} {targetId}",
                    UseShellExecute = false,
                    //RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };
            process.Start();
            
            /*#if DEBUG
            System.Threading.Tasks.Task.Run(() =>
            {
                while (!process.StandardOutput.EndOfStream)
                {
                    var line = process.StandardOutput.ReadLine();
                    Debug.WriteLine(line);
                }
            });
            #endif*/

            return !process.HasExited;
        }
        catch (Exception ex)
        {
            Debug.Write("ExternalProcessManagement: " + ex);
            return false;
        }
    }
}