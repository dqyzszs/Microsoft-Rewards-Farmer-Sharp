﻿using MicrosoftRewardsFarmer.Extensions;
using MicrosoftRewardsFarmer.Models;
using MicrosoftRewardsFarmer.Utilities;
using PuppeteerPlus;
using PuppeteerSharp;

namespace MicrosoftRewardsFarmer;

public class Farmer
{
    #region Constructors

    public Farmer(int id, Account account, string browserPath)
    {
        Id = id;
        _account = account;

        if (account.Username == "")
            throw new ArgumentException(Resources.Farmer.Empty_username);

        Name = _account.Username![.._account.Username!.IndexOf('@')];

        if (Configuration.Headless && account.Password == "")
            throw new ArgumentException(Resources.Farmer.Empty_password);

        // Decode the credential
        _account.Decode();

        if (string.IsNullOrEmpty(browserPath))
            throw new ArgumentException("browserPath");

        _browserPath = browserPath;
    }

    #endregion

    #region Variables

    public readonly int Id;
    public readonly string Name;
    private readonly string _browserPath;
    private readonly Account _account;

    #endregion

    #region Properties
    
    public int UserPoints { get; private set; }
    
    #endregion

    #region Methods

    /// <summary>
    ///     Start farming
    /// </summary>
    /// <exception cref="Exception"></exception>
    public async Task<ProgressDetail> StartAsync(IProgress<ProgressDetail> progress)
    {
        var progressModel = new ProgressDetail
        {
            Detail = Resources.Farmer.Initialization + "...",
            Current = 0,
            Total = 6
        };
        progress.Report(progressModel);
        var progressString = new Progress<string>();
        progressString.ProgressChanged += (_, detail) => {
            progressModel.Detail = detail;
            progress.Report(progressModel);
        };
        
        var userDataPath = string.Empty;
        if (Configuration.Profil)
        {
            userDataPath = AppPath.GetFullPath("Profils/" + _account.Username.ToLower());
            Utility.RemoveLastSession(userDataPath);
        }
        await using var browser = await Utility.StartNewBrowserAsync(
            _browserPath,
            Configuration.BrowserArguments,
            Configuration.Headless,
            userDataPath);
        // TODO Doesn't work on linux
        ExternalProcessManagement.RegisterForClosing(browser.Process);

        var page = await GetNewPageAsync(browser);

        // Set timeout
        if (Configuration.Timeout >= 0)
            browser.DefaultWaitForTimeout = Configuration.Timeout;

        // LogIn
        var session = new Session(Name, page);

        await ConnectToMicrosoft(page, session, _account, progressString);
        progress.Report(progressModel.SetProgress(Resources.Farmer.Initialization + "...", 1));

        // Saving Bing session
        if (Configuration.Session)
        {
            await Bing.GoToBingAsync(page);
            await session.SaveAsync();
        }

        // Get account points
        UserPoints = await MsRewards.GetRewardsPointsAsync(page, progressString);
        progress.Report(progressModel.SetProgress(2));

        // Resolve cards
        await ResolveCardsAsync(page, progressString);
        progress.Report(progressModel.SetProgress(3));

        // Saving MsRewards session
        await session.SaveAsync();

        // Run searches
        var searchProgress = new Progress<ProgressDetail>();
        searchProgress.ProgressChanged += (_, sP) => {
            progressModel.SetProgress(sP.Detail, 3 + sP.Current);
        };
        await RunSearches(page, searchProgress);

        // Get end points
        var endRewardPoints = await MsRewards.GetRewardsPointsAsync(page, progressString);
        progress.Report(progressModel.SetProgress(6));

        return UserPoints <= 0
                            ? progressModel.SetProgress(string.Format(Resources.Farmer.Done_Total, endRewardPoints))
                            : progressModel.SetProgress(string.Format(Resources.Farmer.Done_Gain_Total,
                                                                      endRewardPoints - UserPoints, endRewardPoints));
    }

    private static async Task<IPage> GetNewPageAsync(IBrowser browser)
    {
        // Get the first page
        var page = await browser.NewPageAsync();

        // Apply user agent
        await page.SwitchToDesktopAsync();
#if DEBUG
        // Redirect browser page console to app debug console
        page.SetConsoleOutputToDebugger();
#endif
        return page;
    }

    private static async Task ConnectToMicrosoft(IPage page, Session session, Account account, IProgress<string> progress)
    {
        bool connected = false;
        
        // Profil?
        if (!Configuration.Session)
        {
            await page.TryGoToAsync(Bing.LoginUrl, WaitUntilNavigation.Networkidle2);
            
            // Check if we are not in the logins screen
            while (true) try
            {
                if (await page.QuerySelectorAsync(".logo") == null)
                    // Wait to be fully connected
                    await page.WaitForSelectorAsync("#microsoft", new WaitForSelectorOptions()
                    {
                        Visible = true,
                        Timeout = 300000
                    });
                
                break;
            }
            catch (EvaluationFailedException)
            {
                await page.WaitForPageToLoadAsync();
            }

            connected = page.Url.StartsWith("https://account.microsoft.com");
        }
        // Session
        else if (session.Exists())
        {
            progress.Report(Resources.Farmer.Restoring_session + "...");

            if (await Bing.GoToBingAsync(page))
            {
                await session.RestoreAsync();

                // Thanks microsoft to put rewards on the same domaine (irony)
                if (await page.TryGoToAsync(MsRewards.URL + "welcome", WaitUntilNavigation.Networkidle0))
                    connected = await session.RestoreAsync();
            }
        }

        if (!connected)
            await Bing.SignInToMicrosoftAsync(page, account, progress);
    }

    private static async Task ResolveCardsAsync(IPage page, IProgress<string> progress)
    {
        // There's no way of knowing how long it will take, so we wait 4 tries before giving up.
        int trys = 4;

        while (trys-- > 0)
        {
            var cards = await MsRewards.GetCardsAsync(page, progress);

            if (cards.Length > 0)
                await MsRewards.ProceedCardsAsync(page, cards, progress);
            else
                trys = 0; // Break
        }
    }

    private static async Task RunSearches(IPage page, IProgress<ProgressDetail> progress)
    {
        // There's no way of knowing how long it will take, so we wait 4 tries before giving up.
        int trys = 4;
        var progressDetail = new ProgressDetail(2);
        var progressString = new Progress<string>();
        progressString.ProgressChanged += (_, msg) =>
        {
            progressDetail.SetProgress(msg);
            progress.Report(progressDetail);
        };

        while (trys-- > 0)
        {
            // Get remaining searches to do
            progressDetail.SetProgress(Resources.MsRewards.Searches_points);
            progress.Report(progressDetail);
            var points = await MsRewards.GetRewardsSearchPointsAsync(page);

            if (points == null)
            {
                // Default points TODO: User can input manually number of total points (especially for US user)
                points = new SearchesPointsModel(
                    // (Mobile, Desktop, Edge)
                    new SearchPointsModel(0, 0, 0), // Current
                    new SearchPointsModel(60, 90, 12) // Total
                );
                trys = 0;
            }

            double remainingDesktopSearch = points.Value.Total.DesktopSearch - points.Value.Current.DesktopSearch;
            double remainingMobileSearch = points.Value.Total.MobileSearch - points.Value.Current.MobileSearch;

            // If not searches left, we call it a day
            if (remainingDesktopSearch + remainingMobileSearch == 0)
                trys = 0; // Break

            // Desktop search
            if (remainingDesktopSearch > 0)
            {
                await page.SwitchToDesktopAsync();
                await Bing.RunSearchesAsync(
                    page,
                    // 90 points max / 3 points per page
                    Convert.ToByte(Math.Ceiling(remainingDesktopSearch / 3)),
                    progressString); //TODO progress
            }

            progressDetail.SetProgress(1);
            progress.Report(progressDetail);

            // Mobile search
            if (remainingMobileSearch > 0)
            {
                await page.SwitchToMobileAsync();
                await Bing.RunSearchesAsync(
                    page,
                    // 60 points max / 3 points per page
                    Convert.ToByte(Math.Ceiling(remainingMobileSearch / 3)),
                    progressString); //TODO progress
            }

            progressDetail.SetProgress(2);
            progress.Report(progressDetail);
        }
    }
    
    #endregion
}