namespace MicrosoftRewardsFarmer.Enumerations;

public enum BingSearchType
{
    Desktop,
    Mobile
}