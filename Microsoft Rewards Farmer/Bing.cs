﻿using System.Web;
using MicrosoftRewardsFarmer.Enumerations;
using MicrosoftRewardsFarmer.Exceptions;
using MicrosoftRewardsFarmer.Extensions;
using MicrosoftRewardsFarmer.Models;
using OtpNet;
using PuppeteerSharp;
using PuppeteerPlus;
using RandomWordGenerator;

namespace MicrosoftRewardsFarmer;

public static class Bing
{
    #region Variables

    public const string URL = "https://www.bing.com/";
    public const string LoginUrl = "https://login.live.com/";
    public const string AccountUrl = "https://account.live.com/";
    #endregion

    #region Methods

    /// <summary>
    /// Sign in to Bing
    /// </summary>
    /// <param name="page">Page to use for the login process</param>
    /// <param name="account">Account to login to</param>
    /// <param name="progress">Progress report</param>
    /// <exception cref="Exception">Sign in error</exception>
    // TODO #1 Do not do check in order, but globally.
    public static async Task SignInToMicrosoftAsync(IPage page, Account account, IProgress<string> progress)
    {
        progress.Report(Resources.Bing.SignIn + "...");

        IElementHandle element;

        if (!await page.TryGoToAsync(LoginUrl, WaitUntilNavigation.Networkidle0))
            throw new Exception(string.Format(Resources.Bing.SignIn_error_X, Resources.Bing.Navigation_failed));

        // Check for login spam protection
        await CheckBingLoginErrorAsync(page);
        
        // TODO #1 This what I mean, The user is actually connected, but microsoft ask the user some check, this mean we don't need to do all the login process, just the check
        // User warning
        if (page.Url.StartsWith(AccountUrl + "Abuse") && page.Url.StartsWith(AccountUrl + "identity"))
            goto pleaseRemoveThatLater;

        // Enter Username
        await page.WaitForSelectorAsync("input[name = \"loginfmt\"]", new WaitForSelectorOptions()
        {
            Timeout = 300000
        });
        await FillSignInFormAsync(page, "input[name = \"loginfmt\"]", account.Username, "#usernameError");

        // Enter password
        if (account.Password != "")
            await FillSignInFormAsync(page, "input[name = \"passwd\"]", account.Password, "#passwordError");
        else
            await page.WaitForSelectorToHideAsync("input[name = \"passwd\"]", true, 0);

        await page.WaitTillHTMLRenderedAsync();

        // Microsoft Authenticator
        // "Don't ask me about this device again" check box
        element = await page.QuerySelectorAsync("#idChkBx_SAOTCAS_TD");
        if (element != null)
            await ProceedMicrosoftAuthenticator(page, element, progress);

        // OTP
        element = await page.QuerySelectorAsync("#idTxtBx_SAOTCC_OTC"); // Code text box
        if (element != null)
            await ProceedOTP(page, account, progress);

        // Check your identity
        while (await page.QuerySelectorAsync("#idDiv_SAOTCS_Title") != null) // Code text box
            await ProceedIdentityCheck(page, progress);

        pleaseRemoveThatLater:
        
        // User warning - abuse
        if (page.Url.StartsWith(AccountUrl + "Abuse"))
            await ProceedAbuseCheck(page, progress);

        // User warning - bot check (This one we will need the email account of the user :/ )
        if (page.Url.StartsWith(AccountUrl + "identity"))
            await ProceedBotCheck(page, progress);

        // Oops
        await CheckBingLoginErrorAsync(page);

        // Remind password
        await page.WaitForPageToLoadAsync();
        var success = false;

        // When the page take time sometime this is skipped
        // TODO Better login screen detection
        while (!success && page.Url.StartsWith(LoginUrl))
            try
            {
                success = await ProceedRememberMe(page);

                break;
            }
            catch (PuppeteerException) { }
        
        await page.WaitForNavigationAsync(new NavigationOptions()
        {
            Timeout = 300000
        });
        // This avoids the famous login loop, because it takes time for Microsoft Login to complete the login procedure
        await page.WaitForSelectorAsync("#microsoft", new WaitForSelectorOptions()
        {
            Visible = true,
            Timeout = 300000
        });
    }

    private static async Task CheckBingLoginErrorAsync(IPage page)
    {
        if (await page.QuerySelectorAsync("#idTD_Error") != null)
        {
            var messageTitle = await page.GetPropertyValueAsync<string>("innerText", "#idTD_Error");
            var messageInfo = await page.GetPropertyValueAsync<string>("innerText", "#error_Info");
            throw new Exception(string.Format(Resources.Bing.SignIn_error_X, messageTitle + " " + messageInfo));
        }
    }

    /// <summary>
    /// Fill the sign in form
    /// </summary>
    /// <param name="page">Page to fill the form</param>
    /// <param name="selector">Input to fill</param>
    /// <param name="value">Value to put inside the input field</param>
    /// <param name="errorSelector">Selector to the error element</param>
    /// <exception cref="Exception">Sign in error</exception>
    private static async Task FillSignInFormAsync(IPage page, string selector, string value, string errorSelector)
    {
        IElementHandle element;
        var success = false;
        //int trys = 3;

        while (!success && page!.Url.StartsWith(LoginUrl))
            try
            {
                await page.WaitForPageToLoadAsync();
                
                // If selector exist
                if (await page.QuerySelectorAsync(selector) == null) break;

                // Fill the input field and go next
                await page.ReplaceAllTextAsync(selector, value);
                element = await page.WaitForSelectorAsync("input[id = \"idSIButton9\"]");
                await element.ClickAsync();

                // Wait for animation to finish
                success = await page.WaitForSelectorToHideAsync(selector, true, 10000);

                // Check for error msg
                await page.WaitForPageToLoadAsync();
                element = await page.QuerySelectorAsync(errorSelector);
                if (element != null)
                {
                    var error = await page.GetPropertyValueAsync<string>("innerText", errorSelector);

                    // This is when Bing fail to connect but this is very badly implemented
                    /*trys--;
                    if (trys < 1)*/
                    throw new Exception(string.Format(Resources.Bing.SignIn_error_X, error));
                }
            }
            catch (PuppeteerException) { }
    }

    /// <summary>
    /// Proceed the Microsoft Authenticator login screen
    /// </summary>
    /// <param name="page"></param>
    /// <param name="element"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    private static async Task ProceedMicrosoftAuthenticator(IPage page, IElementHandle element, IProgress<string> progress)
    {
        // Click on check box
        await element.ClickAsync();

        // Get description
        var messageTitle =
            await page.GetPropertyValueAsync<string>("innerText", "#idDiv_SAOTCAS_Description");
        progress.Report(messageTitle);

        // Wait for the check box to disapear
        await page.WaitForSelectorToHideAsync("#idChkBx_SAOTCAS_TD", false, 70000);

        // Timeout check
        element = await page.QuerySelectorAsync("#idDiv_SAASTO_Description"); // Get description
        if (element != null)
            throw new Exception(string.Format(Resources.Bing.SignIn_error_X,
                                              await element.GetPropertyValueAsync<string>("innerText")));
    }

    /// <summary>
    /// Proceed the OTP login scren
    /// </summary>
    /// <param name="page"></param>
    /// <param name="account"></param>
    /// <param name="progress"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    private static async Task ProceedOTP(IPage page, Account account, IProgress<string> progress)
    {
        // Click the "Don't ask me about this device again" check box
        await (await page.QuerySelectorAsync("#idChkBx_SAOTCC_TD")).ClickAsync();

        if (string.IsNullOrEmpty(account.OTP))
        {
            if (Configuration.Headless) // No user and no OTP = Stop
                throw new Exception(string.Format(Resources.Bing.SignIn_error_X, Resources.Bing.no_OTP));

            // Warn user and let it put his code
            var description =
                await page.GetPropertyValueAsync<string>("innerText", "#idDiv_SAOTCC_Description");
            progress.Report(description);
        }
        else // Use the given OTP code
        {
            progress.Report(Resources.Bing.OTP_auth);

            // Get user OTP
            var bytes = Base32Encoding.ToBytes(account.OTP);
            var totp = new Totp(bytes, mode: OtpHashMode.Sha1);
            var totpCode = totp.ComputeTotp();
            await page.ReplaceAllTextAsync("#idTxtBx_SAOTCC_OTC", totpCode);

            // Click on "Check" button
            await (await page.QuerySelectorAsync("#idSubmit_SAOTCC_Continue")).ClickAsync();
        }

        do
        {
            // Wait for refresh
            await page.WaitTillHTMLRenderedAsync();

            // If the code is wrong or abused
            IElementHandle element = await page.QuerySelectorAsync("#idSpan_SAOTCC_Error_OTC"); // Error message
            if (element != null)
            {
                var error = await element.GetPropertyValueAsync<string>("innerText");

                if (Configuration.Headless)
                    throw new Exception(string.Format(Resources.Bing.SignIn_error_X, error));
                progress.Report(error);
            }

            // Wait for the "Check" button to disapear
            await page.WaitForSelectorToHideAsync("#idSubmit_SAOTCC_Continue", false, 500000);
        } while (!Configuration.Headless &&
                 await page.QuerySelectorAsync("#idSubmit_SAOTCC_Continue") != null);

        progress.Report(Resources.Bing.SignIn + "...");
    }

    /// <summary>
    /// Proceed the bot check login screen
    /// </summary>
    /// <param name="page"></param>
    /// <param name="progress"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    private static async Task ProceedBotCheck(IPage page, IProgress<string> progress)
    {
        var message = await page.GetPropertyValueAsync<string>("innerText", "#iSelectProofDesc");

        if (Configuration.Headless) throw new Exception(string.Format(Resources.Bing.SignIn_error_X, message));

        // If not in headless mode, let user fix the issue
        progress.Report(message);

        await page.WaitForPageToExitAsync(AccountUrl + "identity");
    }

    /// <summary>
    /// Proceed the identity check login screen
    /// </summary>
    /// <param name="page"></param>
    /// <param name="progress"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    private static async Task ProceedIdentityCheck(IPage page, IProgress<string> progress)
    {
        var message = await page.GetPropertyValueAsync<string>("innerText", "#idDiv_SAOTCS_Title");

        if (Configuration.Headless) throw new Exception(string.Format(Resources.Bing.SignIn_error_X, message));

        // If not in headless mode, let user fix the issue
        progress.Report(message);

        // Wait for the Check button to appear
        await page.WaitForSelectorAsync("#idDiv_SAOTCC_Title",
                                        new WaitForSelectorOptions { Timeout = 0, Visible = true });

        // Wait for the Check button to disapear
        await page.WaitForSelectorToHideAsync("#idDiv_SAOTCC_Title", true, 0);
    }

    /// <summary>
    /// Procced the abuse check login screen
    /// </summary>
    /// <param name="page"></param>
    /// <param name="progress"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    private static async Task ProceedAbuseCheck(IPage page, IProgress<string> progress)
    {
        // (We might be able to find a phone number generator API like quackr.io) TODO: Auto account unlocker
        var message = await page.GetPropertyValueAsync<string>("innerText", "#StartHeader");

        if (Configuration.Headless) throw new Exception(string.Format(Resources.Bing.SignIn_error_X, message));

        // If not in headless mode, let user fix the issue
        progress.Report(message);

        var element = await page.WaitForSelectorAsync("#FinishAction",
                                                    new WaitForSelectorOptions
                                                    { Timeout = 0, Visible = true });
        if (element != null)
            await element.ClickAsync();

        await page.WaitForSelectorToHideAsync("#FinishAction", true, 4000);
    }

    /// <summary>
    /// Proceed the remenber me login screen (and say yes to the remember me)
    /// </summary>
    /// <param name="page"></param>
    /// <returns></returns>
    private static async Task<bool> ProceedRememberMe(IPage page)
    {
        var element = await page.QuerySelectorAsync("input[name = \"DontShowAgain\"]");
        if (element == null) return false;

        await element.ClickAsync();

        await page.WaitForSelectorAsync("input[name = \"DontShowAgain\"]",
                                                   new WaitForSelectorOptions { Timeout = 600000 });
        await page.ClickAsync("input[id = \"idSIButton9\"]");

        return await page.WaitForSelectorToHideAsync(
            "input[name = \"DontShowAgain\"]", true, 4000);
    }

    /// <summary>
    /// Run bing search
    /// </summary>
    /// <param name="page">The page to run searches in</param>
    /// <param name="type">TType of searches to be done</param>
    /// <param name="progress">Progress report</param>
    /// <param name="numOfSearches">Number of searches to be performed</param>
    public static async Task RunSearchesAsync(IPage page, byte numOfSearches, IProgress<string> progress)
    {
        var type = await page.IsMobileAsync() ? BingSearchType.Mobile : BingSearchType.Desktop;
        var typeString = type switch
        {
            BingSearchType.Desktop => Resources.Bing.Mobile.ToLower(),
            BingSearchType.Mobile => Resources.Bing.Desktop.ToLower(),
            _ => string.Empty
        };
        
        progress.Report(string.Format(Resources.Bing.Generate_X_searches, typeString) + " ...");

        const string url = URL + "search?q=";
        var terms = RandomWord.GetWords(numOfSearches);

        for (var i = 0; i < terms.Length; i++)
        {
            progress.Report(string.Format(Resources.Bing.Running_X_searches, typeString) + $" {i}/{numOfSearches}");
            await page.TryGoToAsync(url + terms[i], WaitUntilNavigation.Networkidle2);
            await CheckBingReadyAsync(page, !Configuration.Unit_test);
        }
    }

    /// <summary>
    /// Go to Bing
    /// </summary>
    /// <param name="page">The page to go to Bing</param>
    /// <returns>If success</returns>
    public static async Task<bool> GoToBingAsync(IPage page)
    {
        if (!await page.TryGoToAsync(URL, WaitUntilNavigation.Networkidle0)) return false;
        
        //await page!.WaitTillHTMLRendered();
        await CheckBingReadyAsync(page, false);
        return true;
    }

    /*/// <summary>
    /// Check if their is an active session, on the current page.
    /// Warning: If your not on a Bing page, false will be returned.
    /// </summary>
    /// <param name="page"></param>
    /// <returns></returns>
    /// <exception cref="NotImplementedException"></exception>
    public static async Task<bool> IsConnected(IPage page)
    {
        /*var bingCookies = await page.GetCookiesAsync(".bing.com");

        if (bingCookies.Length == 0) return false;

        var anonBingCookies = bingCookies.Select(cookie => cookie.Name == "ANON");

        return anonBingCookies.Any(); #1#
        using var httpClient = new HttpClient();
        
        /*

        var request = await httpClient.GetAsync(LoginUrl);
        
        var body = request.Content;
        
        var stringBody = await body.ReadAsStringAsync();
        
        return request.StatusCode == HttpStatusCode.RedirectMethod;#1#
        
        var request = await httpClient.GetAsync(
            URL +
            "fd/auth/signin?action=interactive&provider=windows_live_id&return_url=https%3a%2f%2fwww.bing.com"
        );
        
        
        var stringBody = await request.Content.ReadAsStringAsync();
        
        return request.StatusCode == HttpStatusCode.RedirectMethod;
    }*/

    /// <summary>
    /// Check if bing is ready
    /// </summary>
    /// <param name="page"></param>
    /// <param name="checkConnected">Check if the user is connected</param>
    /// <exception cref="NotConnectedException">Throw if there is no connected account. This only append if checkConnected is true</exception>
    /// <exception cref="Exception"></exception>
    public static async Task CheckBingReadyAsync(IPage page, bool checkConnected = true)
    {
        IElementHandle element;

        if (await page.IsMobileAsync())
        {
            // In mobile mode, it doesn't have intrusive things
        }
        else
        {
            // If not connected to MR, connect to MR
            // * Test this because their is a bug with bing (Error 400)
            element = await page.QuerySelectorAsync("a[onclick=\"setsrchusr()\"]");
            if (element != null /*&& await IsConnected(page)*/)
            {
                // This page is so wrong, I have no idea why (Micro$oft?)
                // When clicking on connect button we go into a 400 error page, but we actually successfully connect
                // So we juste need to redirect our self into the page we originally want

                // https://www.bing.com/rewards/signin?ru= {encoded redirection link}
                var encodedUrl = page.Url.Remove(0, 39);
                var url = HttpUtility.UrlDecode(encodedUrl);

                await element.ClickAsync();
                await page.WaitForSelectorToHideAsync("a[onclick=\"setsrchusr()\"]");
                await page.TryGoToAsync(url, WaitUntilNavigation.Networkidle0);

                // TODO: Avoid self call
                await CheckBingReadyAsync(page, checkConnected); // To be sure the page what we wanted it's clean
                return;
            }

            // If cookies, eat it !                
            element = await page.QuerySelectorAsync("#bnp_cookie_banner");
            if (element != null) await element.RemoveAsync();

            // If Bing Wallpaper, kill it without mercy !
            element = await page.QuerySelectorAsync("#b_notificationContainer_bop");
            if (element != null) await element.RemoveAsync();

            // If not connected, connect to Bing
            element = await page.QuerySelectorAsync("#id_a");
            if (checkConnected && element != null && await element.IsVisibleAsync())
            {
                var url = page.Url;
                await page.TryGoToAsync(
                    URL +
                    "fd/auth/signin?action=interactive&provider=windows_live_id&return_url=https%3a%2f%2fwww.bing.com",
                    WaitUntilNavigation.Networkidle0);
                await page.WaitTillHTMLRenderedAsync();

                // Yes I look good
                element = await page.QuerySelectorAsync("#iLooksGood");
                if (element != null)
                    await element.ClickAsync();

                // If we end up in the login page
                if (page.Url.StartsWith(LoginUrl))
                    throw new NotConnectedException("Not connected to Bing");

                await page.TryGoToAsync(url, WaitUntilNavigation.Networkidle0);
                // TODO: Avoid self call
                await CheckBingReadyAsync(page, checkConnected); // To be sure the page we wanted it's clean
            }

            // Oh poop
            if (page.Url.StartsWith(AccountUrl + "proofs/Verify"))
                throw new Exception("Bing error - This account must be verified.");
        }
    }

    #endregion
}