namespace MicrosoftRewardsFarmer.Models;

public struct ProgressDetail
{
    public ProgressDetail(int total)
    {
        Detail = string.Empty;
        Total = total;
        Current = 0;
    }

    public string Detail;
    public int Total;
    public int Current;

    public ProgressDetail SetProgress(string detail, int current)
    {
        Detail = detail;
        Current = current;

        return this;
    }

    public ProgressDetail SetProgress(string detail)
    {
        Detail = detail;

        return this;
    }

    public ProgressDetail SetProgress(int current)
    {
        Current = current;

        return this;
    }
}