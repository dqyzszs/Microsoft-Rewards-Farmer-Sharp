﻿using System.Text;
using MicrosoftRewardsFarmer.Exceptions;
using Newtonsoft.Json;
using Tomlet;
using Tomlet.Attributes;
using Tomlet.Exceptions;

namespace MicrosoftRewardsFarmer.Models;

[Serializable]
public class Settings
{
    #region Properties

    [TomlProperty("Account")]
    public Account[]? Accounts { get; set; }
    [TomlProperty("Logger")]
    public Loggers? Loggers { get; set; }
    [TomlProperty("Reward")]
    public RewardModel[]? Rewards { get; set; }
    public SearchSettingsModel? Search { get; set; }

    #endregion

    #region Methods

    /// <summary>
    /// Get the local app settings
    /// </summary>
    /// <param name="settingsFilePath"></param>
    /// <returns></returns>
    /// <exception cref="SettingsLoadException"></exception>>
    /// <exception cref="JsonSerializationException"></exception>
    /// <exception cref="Exception"></exception>
    public static Settings? GetSettings(string settingsFilePath)
    {
        Settings? settings;
        
        try
        {
            settings = Path.GetExtension(settingsFilePath) switch
            {
                ".toml" => GetSettingsToml(settingsFilePath),
                ".json" => GetSettingsJson(settingsFilePath),
                _ => throw new SettingsLoadException(Resources.Settings.Unknow_Extension, new NotSupportedException())
            };
        }
        catch (NotSupportedException)
        {
            throw new SettingsLoadException(Resources.Settings.Unknow_Extension);
        }
        catch (FileNotFoundException)
        {
            throw new SettingsLoadException(string.Format(
                                               Resources.Settings.Missing_Settings_file,
                                               Path.GetFileName(Configuration.SettingsFilePath),
                                               Path.GetDirectoryName(Configuration.SettingsFilePath) ));
        }
        catch (JsonReaderException e)
        {
            var message = new StringBuilder()
                         .AppendLine(string.Format(
                                         Resources.Settings.Loading_Settings_file_error,
                                         Path.GetFileName(Configuration.SettingsFilePath) ))
                         .AppendLine()
                         .AppendLine(e.Message)
                         .ToString();

            throw new SettingsLoadException(message, e);
        }
        
        if (settings == null)
            throw new SettingsLoadException(string.Format(
                                               Resources.Settings.Empty_Settings_file,
                                               Path.GetFileName(Configuration.SettingsFilePath) ));
        
        if (settings.Accounts == null || settings.Accounts.Length == 0)
            throw new SettingsLoadException(string.Format(
                                               Resources.Settings.Empty_accounts_Settings_file,
                                               Path.GetFileName(Configuration.SettingsFilePath) ));

        return settings;
    }

    /// <summary>
    ///     Get the local app settings
    /// </summary>
    /// <returns>The local app settings</returns>
    /// <exception cref="TomlException">An error occurs while reading TOML text.</exception>
    /// <exception cref="T:System.IO.IOException">An I/O error occurred while opening the file.</exception>
    /// <exception cref="T:System.UnauthorizedAccessException">
    ///     This operation is not supported on the current platform.
    ///     -or-
    ///     <paramref name="settingsFilePath" /> specified a directory.
    ///     -or-
    ///     The caller does not have the required permission.
    /// </exception>
    /// <exception cref="T:System.IO.FileNotFoundException">The file specified in <paramref name="settingsFilePath" /> was not found.</exception>
    /// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission.</exception>
    public static Settings? GetSettingsToml(string settingsFilePath)
    {
        // Check if the settings file exist
        if (!File.Exists(settingsFilePath))
            throw new FileNotFoundException(string.Format(
                                                Resources.Settings.Missing_Settings_file_exception,
                                                Path.GetFileName(settingsFilePath))
            );

        // Read settings file
        var settingsToml = File.ReadAllText(settingsFilePath);

        // Deserialize settings file
        // TODO Don't work
        return TomletMain.To<Settings>(settingsToml);
    }

    /// <summary>
    ///     Get the local app settings
    /// </summary>
    /// <returns>The local app settings</returns>
    /// <exception cref="JsonReaderException">An error occurs while reading JSON text.</exception>
    /// <exception cref="T:System.IO.IOException">An I/O error occurred while opening the file.</exception>
    /// <exception cref="T:System.UnauthorizedAccessException">
    ///     This operation is not supported on the current platform.
    ///     -or-
    ///     <paramref name="settingsFilePath" /> specified a directory.
    ///     -or-
    ///     The caller does not have the required permission.
    /// </exception>
    /// <exception cref="T:System.IO.FileNotFoundException">The file specified in <paramref name="settingsFilePath" /> was not found.</exception>
    /// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission.</exception>
    [Obsolete("Json is not user friendly")]
    public static Settings? GetSettingsJson(string settingsFilePath)
    {
        // Check if the settings file exist
        if (!File.Exists(settingsFilePath))
            throw new FileNotFoundException(string.Format(
                                                Resources.Settings.Missing_Settings_file_exception,
                                                Path.GetFileName(settingsFilePath))
            );

        // Read settings file
        var settingsJson = File.ReadAllText(settingsFilePath);

        // Deserialize settings file
        return JsonConvert.DeserializeObject<Settings>(settingsJson);
    }

    #endregion
}