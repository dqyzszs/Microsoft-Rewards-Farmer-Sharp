﻿using System;

namespace MicrosoftRewardsFarmer.Models;

public struct SearchesPointsModel
{
    #region Constructors

    public SearchesPointsModel(SearchPointsModel current, SearchPointsModel total)
    {
        Current = current;
        Total = total;
    }

    #endregion

    #region Variables

    public readonly SearchPointsModel Current;
    public readonly SearchPointsModel Total;

    #endregion

    #region Methods

    public override string ToString() =>
        "Current( " + Current + " )" + Environment.NewLine +
        "Total( " + Total + " )";

    #endregion
}