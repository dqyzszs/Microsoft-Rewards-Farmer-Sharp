﻿using System;

namespace MicrosoftRewardsFarmer.Models;

[Serializable]
public class RewardModel
{
    public string Title { get; set; } = string.Empty;
    public uint Cost { get; set; } = 0;
    public uint Discounted { get; set; } = 0;
}